package com.eval.evaluation.controller;

import com.eval.evaluation.entity.Album;
import com.eval.evaluation.repository.AlbumRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class AlbumController {
    @Autowired
    private AlbumRepository albumRepository;


    @GetMapping("/albums")
    public List<Album> getAll() {
        return albumRepository.findAll();
    }

    @GetMapping("/albums/{albumId}")
    public Optional<Album> getOne(@PathVariable Long albumId) {
        return albumRepository.findById(albumId);
    }

    @DeleteMapping("/albums/{albumId}")
    public void deleteOne(@PathVariable Long albumId) {
        albumRepository.deleteById(albumId);
    }

    @PostMapping("/albums")
    public Album create(@RequestBody Album newAlbum) {
        return albumRepository.save(newAlbum);
    }

    @PutMapping("/albums/{albumId}")
    public Album update(@PathVariable Long albumId, @RequestBody Album updatedAlbum) {
        Optional<Album> optionalAlbum = albumRepository.findById(albumId);
        var albumupdate= albumRepository.findById(albumId);
        if(albumupdate.isPresent()){
            var currentalbum = albumupdate.get();
            currentalbum.setTitle(updatedAlbum.getTitle());
            currentalbum.setDescription(updatedAlbum.getDescription());
            currentalbum.setImage(updatedAlbum.getImage());
            currentalbum.setDuration(updatedAlbum.getDuration());
            currentalbum.setRelease(updatedAlbum.getRelease());

            return albumRepository.save(currentalbum);
        }else{
            return null;
        }
    }


}
